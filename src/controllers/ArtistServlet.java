package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ArtistDAO;
import models.Artist;

@WebServlet("/artist")
public class ArtistServlet extends HttpServlet {

    private ArtistDAO artistDao = new ArtistDAO();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        long artistId = Long.parseLong(request.getParameter("id"));

        Artist artist = artistDao.findArtist(artistId);
        request.setAttribute("artist", artist);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/artist.jsp");
        dispatcher.include(request, response);
    }

}
